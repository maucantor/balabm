# The {balabm} R package: Agent-based models for sperm whale societies
------------------------------------------------

### **Maintainer and contact** 
Mauricio Cantor

*Department of Biology, Dalhousie University, 1355 Oxford Street, Halifax, NS B3H 4J1, Canada. Email: m.cantor@ymail.com. Telephone: +1 (902) 494-3723*


### **Reference** 
Cantor M, Shoemaker LG, Cabral R, Flores CO, Varga M, Whitehead H. 2015. Multilevel animal societies can emerge from cultural transmission. Nature Communications. 6:8091 doi: 10.1038/ncomms9091



### **Collaborators**
Mauricio Cantor (1), Lauren G. Shoemaker (2), Reniel Cabral (3,4), Cesar O. Flores (5), Melinda Varga (6) & Hal Whitehead (1)

(1) Department of Biology, Dalhousie University, 1355 Oxford Street, Halifax, Canada, NS B3H 4J1.
 
(2) Department of Ecology and Evolutionary Biology, University of Colorado at Boulder, UCB 334, Ramaley Hall, Boulder, Colorado 80309, USA. 

(3) National Institute of Physics, College of Science, University of the Philippines Diliman, Quezon City 1101, Philippines. 

(4) Sustainable Fisheries Group, Bren School of Environmental Science and Management and Marine Science Institute, University of California Santa Barbara, 2400 Bren Hall, California 93106, USA. 

(5) School of Physics, Georgia Institute of Technology, 837 State Street, Atlanta, Georgia 30332-430, USA. 

(6) Department of Physics, University of Notre Dame, 225 Nieuwland Science Hall, Notre Dame, Indiana 46556, USA. 



#### VERSION: *balabm v1.1*


We simulated the interactions of multiple individual whales using an agent-based modelling framework (ABM) to test whether the clan structure observed in the sperm whale society off the Pacific Ocean could arise from their changing vocal behaviour. 


#### **The ODD Protocol for the ABMs**

The agent-bases models (ABM) are  described according to the Overview, Design concepts, Details (ODD) protocol (Grimm et al. 2006 10.1016/j.ecolmodel.2006.04.023) as follows:


#### **a) Purpose** 
The purpose of our models is to test which transmission mechanisms for acoustic behaviour, if any, can give rise to distinct acoustic repertoires, and explain the multilevel social structure observed empirically.

#### **b)  Entities, state variables and scales**
The models have one kind of agent which behaves under realistic life-history parameters: female whales that learn coda types at ages of 0 up to 3 years old. Because males sperm whales lead quasi-solitary lives and rarely produce codas, they were not represented in the models. The agents are characterized by their age (years), their coda repertoire (a vector of frequencies of use of different coda types), and which social unit and vocal clan they belong to (nested categorical variables). The models were explicitly temporally-structured and implicitly spatially-structured. However, we did account for different levels of population mixing (and so implicitly for individual movements), with coda transmission operating among individuals of three different social levels (see ‘d’). Simulations lasted for 700 time steps (years).

#### **c)  Process overview and scheduling**
During each time step, biological processes occurred in the following order: birth, coda repertoire composition and changes (at ages up to 3 years old), social unit membership change (or not), and death. Calves have high probability of staying with their natal group, and migration of individuals among social units is very rare; thus nearly-permanent and nearly-matrilineal social units are an emergent property. In these respects, the models mimic several transmission processes characteristic of some socially complex species. We started with two null models without social learning: in one the agents only learn their codas individually; in the other they receive their mothers’ coda repertoire, representative of genetic inheritance (as well as stable vertical cultural transmission). We then simulated a total of 20 complementary scenarios with combinations of oblique social learning of coda types and transmission biases operating at different social levels (see ‘g’). The environment remains the same during the entire simulation.

#### **d)  Design concepts** 
*Basic principles.* These models mimic several learning processes that can occur in complex societies to test if some of them underlies the social structure. This is an alternative to experimental intervention with large-bodied, free-ranging cetaceans to test the effects of such learning mechanisms, which is logistically challenging if not impractical, and aesthetically and ethically questionable.

*Emergence* There are two emergent properties of the interactions among agents: social units (sets of females and their offspring who stay together along the simulated time) and vocal clans (sets of social units with highly similar vocal repertoires). Social units emerge in all models and vocal clans can be predefined (see ‘g’), can emerge or not. 

*Adaptation* The main process of interest modeled is changes in individual coda repertoires, i.e. in frequencies of use of coda types. Each agent has a repertoire represented by a vector with 62 elements denoting continuous absolute frequencies of different coda types from 0 (absent) to 100 (always performed coda type). Only calf agents change their repertoires, by replacing coda types and frequencies once a year while at ages of 0, 1 and 2. After age 3, all agents’ repertoires were fixed. Depending on the sub model (see ‘g’), the repertoire change occurred according to one of the three main transmission processes: (i) Individual learning—calf agents compose their own coda repertoires, i.e. are assigned to random coda types and frequencies drawn from uniform distribution in [0,100]; (ii) Genetic inheritance (which also could represent vertical social learning)—calf agents receive their mothers’ coda repertoires; and (iii) Oblique social learning—calf agents copy coda types and frequencies from adult agents of different generations, kin-related or not. To the models with oblique social learning, the three following effects were included: (iv) Homophily—calf agents preferentially copy codas from adult agents of social units with the highest repertoire similarity with the calf's social unit's repertoire. (The homophily effect posits that like-minded individuals tend to interact more often; since social learning occurs during social interaction, the homophily effect on learning can be represented as individuals with similar behaviour learning preferentially from each other.); (v) Conformism—calf agents disproportionately copy the most common coda types; and (vi) Symbolic marking—all agents of a given social unit are assigned to a random sequence of 6 coda types with frequency of usage 100 (a “symbol”) at time t=1, to mark the identity of their units; all calf agents from t=1 deliberately copy the “symbol” of the unit they belong to. To account for different degrees of population mixing, we replicated the models with oblique social learning and additional effects (iii-iv) across the three levels of the sperm whale society: (vii) Social units—calf agents randomly copy codas from agents of their own social unit; (viii) Predefined clan—agents were arbitrarily assigned to three clans and calf agents learned only from adult agents of their own clan. (Since clan partition could be driven by non-learning mechanisms, we simulated the pre-existence of clans representing geographically segregated clans such as those that seem to occur in the Atlantic where acoustic variation is driven by spatial isolation. We refer to these as ‘predefined clans’ as opposed to the ‘emergent clans’ that may arise in the simulations due to acoustic similarity.); (ix) Population—calf agents learn from any agent in the population. We combined transmission mechanisms, effects, and social levels in a total set of 20 ABMs (see ‘g’). 

*Objectives* Agents have no objectives; they just learn their coda repertoires according to the learning process assigned.

*Learning* Agents only change their coda repertoires at the age of 0-2 years old (at age of 3, the coda repertoires are fixed) and there is no other adaptive traits changing after this period.

*Prediction* Agents have no memory and make no prediction of how to compose their coda repertoires. Environment does not change and so does not influence learning.

*Sensing* Agents do not need to sense any internal variable. There are no environmental variables.

*Interaction* Interactions among agents are direct and mean calves learning their repertoires from adults according to the learning processes described above. Learning can occur within social unit, across units of the same (geographically pre-defined) vocal clan or all agents in the population (see 'g').

*Stochasticity* Stochastic processes are used to regulate individual learning (random assignment of coda types to an agent) and population dynamics. Population was modelled density-dependent, with birth rates, mortality rates and migration rates were age-dependent, such that the population size fluctuate around the carrying capacity. Calves had higher mortality rates than adults. Migration rates of individuals between units are low and decreased with age. All demographic processes were modeled with demographic stochasticity and parameterized from studies of sperm whales in the Eastern Pacific Ocean .

*Collectives* Aggregations of agents represent emergence of the two levels of social structure: the social unit (related females and their offspring) and the vocal clans (social units with high similar averaged vocal repertoires). Units emerge in all models and vocal clans can be predefined in some of them (see 'g').

*Observation.* At the end of a simulation we observed the distribution of social units and vocal clans and how similar they were regarding average coda repertoire. Matrices of similarity of averaged coda repertoire within and between units and clans can be calculated using the asymmetric weighted Bray-Curtis index of similarity. The partition into vocal clans is infered by modularity metric Q using maximization algorithms. The significance of clan partition is assessed using a theoretical model. Outputs are presented as a list of similarity matrices and summary statistics and graphically (scatterplot of population size over time, histogram of age distribution and unit size, boxplot of social unit repertoire, network of social units connected by repertoire similarity, heat maps for within/between social unit repertoire similarity, cluster dendrogram, and summary plot of averaged repertoire similarity and modularity metrics).

#### **e)  Initialization**
Simulations were initialized with the following parameters based on empirical data (see details, justification, and references below). At the first time step, year t=1, all simulations started with a population of N0=1000 agents, to which ages were randomly assigned from a an negative exponential distribution (so the initial population was mostly young, with initial ages varing from 0 and 70years old), and social unit membership labels were assigned with equal probabilities. Each agent received an empty vector of 62 elements (i.e. coda types) representing their coda repertoire. For each agent, half of the elements in its coda repertoire vector were randomly selected to receive an absolute frequency of usage from a uniform distribution in [0,100] (absent coda type=0; always performed=100). Agents are considered calves when they are 0, 1, and 2 years old, during which changes in the coda repertoire occurred. Adult female agents became sexually mature after 9 years old, stopped reproducing after 41 years old, and lived 70 years on average. Female reproduction rate was age-specific and mortality rates were density-dependent such that the population fluctuated around the carrying capacity (N0) over time. The initial number of social units was based on the initial population size (N0) and empirical average unit size in the Pacific (about 12 members). Social units split in half when double the maximum initial unit size. Calf agents remained in the mother’s social unit since they highly depend on their mothers; and adult agents had low probability of randomly migrating to other social units along their lives (c=0.05). Repertoire changes were represented by replacement of frequencies of coda types and occurred three times for each agent (repertoires were fixed after the age of 3). Newborn agents started the simulation with empty coda repertoires; each simulated year, all calf agents changed their coda repertoires under one of the three main mechanisms—with additional effects or not— operating at one of three social levels (see ‘g’). To account for random errors or deliberate innovations, in all models, calf agents also had low individual learning rate (ilearn = 0.02), i.e., replacing one coda type each year (62 codas*0.02≈1) at random by a frequency drawn from a uniform distribution in [0,100]. 

#### **f)	Input**
The models have no external input data but initial parameters are different in sub models (see 'g')

#### **g)	Submodels**
We created a total of 20 sub models, all of which have the same structure but differ in the way calves compose their coda repertoires. In the first null model (ABM01), calf agents learn their coda repertoire only through individual learning. In the second null model (ABM02), calves receive the exact repertoire of their mothers, mimicking the genetic or vertical-cultural transmission of coda repertoires. In all the following models (ABM03-20), calves change repertoires with oblique social learning, some with combinations of the three transmission biases: homophily (ABM06-08, 15-10); conformism (ABM09-11, 15-17); and symbolic marking (ABM12-14, 18-20). Oblique social learning and its biases occurred within social units (ABM04, 07, 11, 13, 16, 19), across social units of the same predefined clans (ABM05, 10, 14, 17, 20); and in the entire population (ABM03, 06, 09, 12, 15, 18).


#### **Empirical justification for the ABMs**

We built agent-based models based on the following empirical socioecological findings:

*a. Social structure parameters*

i.  Female and immature sperm whales are found in nearly-permanent social units that include related, as well as unrelated, animals [1]. Mature males lead quasi-solitary lives and spend a great part of their lives near the poles, associate only briefly with female social units for mating [1], and rarely perform coda vocalizations [2]. Therefore, males would not play any important role in the behavioural learning mechanisms we addressed here, and thus the simulated social units contained only female agents.

ii.  Calves experience high natal group philopatry, i.e. they remain in the natal social unit since they are highly dependent on their mothers[1,3]. Thus, agents with 0, 1 and 2 years old do not change social unit membership, i.e., the probability of remaining with the mothers in their natal social unit (i.e. retaining the social unit membership label) was rem = 1.

iii. Changes in social unit membership are very rare. Social units are long-term entities that are composed of females and immatures that live and move together for many years and likely their entire lives[1,4]. Thus, adult females have very low probability of migrating to other social units5. There is a rough estimate of 6.3% probability of individuals to be involved in social unit merging, splitting or transferring of individuals within a given year[5]. Thus, in our models there was a very low probability, c = 0.05, of adult agents migrating to another social unit (i.e., of randomly changing their social unit label) through their lives. In the abovementioned study [5], the individuals who changed social units were relatively young females. Thus, in our models the migration probability was age-dependent, inversely proportional to the agents’ age, with youngster agents more prone to change their social unit.

iv.  Therefore, with i, ii, and iii, the social units emerge in the simulations as nearly-permanent and nearly-matrilineal, as in the empirical data. Although four other short-term aggregative levels have been described for sperm whales, none of them are considered a social level per se[1] either because they are temporary (small temporal scale: clusters, groups) or because they do not involve social interaction among individuals (large spatial scale: aggregations, concentrations). Clusters are ephemeral sets of individuals swimming closely at the water surface for periods of minutes, and groups are sets of units moving together in a coordinate manner for periods of few hours to few days. Concentrations are patches of whales spanning a few hundred kilometers, within which aggregations of individuals within 10-20 kilometers may occur. Therefore, these short-term aggregative levels were not represented in the models.

*b. Population parameters*

v.  Empirical social units were delineated using individuals re-sighted at least three times, with at least two other individuals during at least two 12h-identification periods spaced out by at least 30 days [5]. Off the Galápagos Islands, there were 174 photo-identified individual sperm whales that meet the above criteria and were assigned to 18 known social units5. To account for undersampling, the simulated initial population size was more than 5 times larger (N0 = 1000 individuals) than the empirical number of photo-identified individuals, but still a reasonable number of sperm whales (excluding mature males) to be using the waters off the Galápagos Islands [6].

vi.  The mean social unit size off the Galápagos Islands is about 12 members (including sometimes juvenile males)[6,7]. The maximum initial number of simulated social units was a function of the initial population size: TVU = N0 / SU where TVU is the total number of social units at time step t=1, N0 is the initial population size, and SU is a random integer from the uniform distribution in [8,14].

vii.  Social units rarely split, merge, or experience abrupt membership changes [1,5]. With stochastic demographic processes in the simulations, social units may increase or decrease in size over the simulated time. To prevent the simulated social units from growing indefinitely, they split in half when their size doubled the maximum initial social unit size at time step t=1. All agents of a splitting social unit had the same probability of remaining in their original unit or being part of a new social unit. That is, half of the agents of that social unit were randomly selected with equal probabilities to keep their social unit membership labels, while the other half had their social unit membership replaced by the new label.

viii. Although precise ages are hard to estimate, female sperm whales are thought to become sexually mature after about the age of 9, stop reproducing after about 40 years old, and live 70 years on average [1]. In the simulations, agents followed these characteristics. The simulation were started with agents’ age being randomly assigned from an exponential distribution (f(X)= λ x e^((-λ x X))}, X ≥ 0, λ = 0.08), so the initial population was mostly young, with ages typically varying from 0 to 70 years old. Agents reproduced from 9 to 40 years old (i.e. newborn calf agents are added to their simulated social units), and demographic rates were chosen such that most whales lived 70 years (although some could die earlier and a few live longer).

ix. The empirical female sperm whale reproduction rate is age-specific and estimated to be bemp-i = 0.257 – (0.0038 × ai) [7], where bemp-i is the empirical birth rate and ai is the age in years of the individual i. Because we were interested only in female agents, we simulated female offspring only, so the simulated birth rate was half of the empirical one (bi = bemp-i/2), assuming a 1:1 sex ratio in birth events. At each time step t, a birth probability bi_t was calculated for all agents of the population who reached sexual maturity, which have the same probability of giving birth. In the social unit of the selected reproductive agents there will be an addition of one agent with age 0 and an empty coda repertoire vector.

x. Population was modeled as density-dependent. We assumed that all social unit members compete for the same foraged resources, and thus the population fluctuated around the carrying capacity (i.e., the initial population size, N0 = 1000 individuals) over time.

xi. Mortality rates (mt) were defined as mt = [(Bt + Nt) – N0] / (Bt + Nt), where Bt is the number of births in year t, Nt is the population size in year t, and N0 is the initial population size. Calves usually have higher mortality than adults, and classic and recent empirical estimates corroborate this pattern9,10. In our models, to account for age-dependence in mortality we calculated the number of agents that would die in each time step, mt, and removed agents from the simulation according to empirical estimates of mortality rates[9] (calf = 0.093/year; adult and juveniles = 0.055/year). Therefore, all agents in the population at the time step t had a probability of dying according to their age class. The death of selected agents was represented by the removal of their coda repertoires, social unit membership, and age labels from the model.

xii. The simulations lasted for t = 700 years, a period 10 times longer than the empirical life expectancy [1] and long enough to achieve a steady state in the simulations.

*c. Learning parameters*

xiii.  While sperm whales mainly produce echolocation sounds (continuous series of clicks), they also produce codas, stereotypical patterns of 3-40 broadband clicks lasting less than 3 seconds in total[11]. Codas are performed in a social context[1,12,13] almost exclusively by females[2] and their major function seems to be labelling and reinforcing social bonds[14].

xiv.  Sperm whale social units have vocal repertoires of about 31 coda types (usually 3-4 commonly used plus about 25 to 28 uncommonly used coda types) [13,15]. In our simulations, agent calves started with coda repertoires of 31 random codas (frequencies of which were drawn from the uniform distribution in [0,100]). Along the simulation, they could change their repertoires and add frequencies of up to 62 coda types, which is twice as long as the empirical coda repertoires to correct for undersampling of empirical data.

xv.  Individuals gradually develop their codas in early ages[16,17] and rarely change coda repertoire as adults. The precise age for learning the acoustic repertoire is genuinely difficult to estimate given the impossibilities of experimental intervention with sperm whales in the wild. While the empirical evidence is inherently scarce, it suggests that individuals mainly compose their vocal repertoires at early ages, rather than over the course of their lives. Based on the best available empirical data, we defined a calf as an agent up to 3 years old—since after this age sperm whales are usually larger and begin to wean and perform deeper dives[4]. Learning was modelled such that calf agents change their coda repertoires between the ages of 0 and 3 years old.  

xvi.  To account for copying errors and innovations[18,19], in all models calf agents were subjected to a low probability (0.02) of replacing the frequency of one randomly chosen coda type (62 codas * 0.02 ≈ 1) by a value drawn from the uniform distribution ϵ[0,100] each year (at ages 0, 1 and 2 year old). This effect represented individual learning, i.e. the chance of deliberately creating new coda types (innovation), or randomly creating a new type by copying an existent one with low fidelity (copying error). Therefore, with xii and xiii, changes in the coda repertoire occurred three times for each calf agent and after the age of 3, their repertoires were fixed.

xvii.  Individuals within social units have very similar (but not identical) coda repertoires[15,17,20,21[]; and social units of the same clans have similar (but not identical) coda repertoires[15,17]. Therefore, just as with the empirical data15, we evaluated possible clan segregation based on the coda repertoire of the social units. In our simulations, we used the averaged coda repertoires of the social units to examine whether they cluster in the simulated acoustic networks (i.e., by testing for modularity).

vxiii.	Social units from different clans in the same area have very distinct coda repertoires [15], which justifies clustering of social units with high coda repertoire similarity as emergent clans in the simulations.

xix.  Demographic effects and individual movements—such as migration, immigration and dispersal—can be important drivers of cultural diversity[22]. Our models were explicitly temporally-structured and implicitly spatially-structured. To account for the potential demographic and movement effects in coda transmission, while respecting the structure of the sperm whale societies with nearly-permanent social units, we simulated different levels of population mixing. We replicated all the models with social learning and transmission biases in the three social levels that are relevant for sperm whales: social unit, predefined geographical clans and population. In doing so, it is reasonable to assume that the mean distance between an individual and other members of its unit is less than that between the individual and other members of its clan, which in turn is less than the distance between two members of the whole population. We represented the case in which social units are fairly closed structures by allowing the calf agents to learn only within their own social units. We represented learning between social units that are spatially segregated by starting the simulations with predefined clans mimicking the geographically-segregated clans found in the Atlantic[2]3, and allowing calf agents to copy from agents of other units, but only from those within their clans. Finally, we represented the case of a completely mixed population by allowing the calf agents learn from any agent in the population.

*References*

1. Whitehead, H. Sperm Whales: Social Evolution in the Ocean. (Chicago University Press, 2003).

2. Marcoux, M., Whitehead, H., & Rendell, L. Coda vocalizations recorded in breeding areas are almost entirely produced by mature female sperm whales (*Physeter macrocephalus*). Can. J. Zool. 84, 609-614 (2006). 

3. Gero, S., Gordon, J. & Whitehead, H. Calves as social hubs: dynamics of the social network within sperm whale units. Proc. R. Soc. B. 280, 20131113 (2013).

4. Gero, S., Milligan, M., Rinaldi, C., Francis, P., Gordon, J., Carlson, C., Steffen, A., Tyack, P., Evans, P. & Whitehead, H. Behavior and social structure of the sperm whales of Dominica, West Indies. Mar. Mamm. Sci. 30, 905-922 (2014).

5. Christal, J., Whitehead, H. & Lettevall, E. Sperm whale social units: variation and change. Can. J. Zool. 76, 1431-1440 (1998).

6. Whitehead, H., Christal, J. & Dufault S. Past and distant whaling and the rapid decline of sperm whales off the Galapagos Islands. Conserv. Biol. 11, 1387-1396 (1997).

7. Whitehead, H., Antunes, R., Gero, S., Wong, S. N. P., Engelhaupt, D., & Rendell, L. Multilevel societies of female sperm whales (*Physeter macrocephalus*) in the Atlantic and Pacific: Why are they so different? Intl. J. Primatol. 33, 1142-1164 (2012).

8. Whitehead, H. Estimates of the current global population size and historical trajectory for sperm whales. Mar. Ecol. Prog. Ser. 242, 295-304 (2002).

9. Chiquet, R. A., Ma, B., Ackleh, A. S., Pal, N. & Sidorovskaia, N. Demographic analysis of sperm whales using matrix population models. Ecol. Model. 248, 71-79 (2013).

10. Whitehead, H. & Gero, S. Positive observed rates of increase do not reflect a healthy population: conflicting rates of increase in the sperm whale population of the eastern Caribbean. Endanger. Species Res. doi: 10.3354/esr00657 (in press).

11. Watkins, W. A. & Schevill, W. E. Sperm whale codas. Journal of Acoustical Society of America. 62, 1486-1490 (1977).

12. Whitehead, H. & Weilgart, L. Patterns of visually observable behavior and vocalizations in groups of female sperm whales. Behaviour 118, 271-296 (1991).

13. Weilgart, L. & Whitehead, H. Group-specific dialects and geographical variation in coda repertoire in South Pacific sperm whales. Behav. Ecol. Sociobiol. 40, 277-285 (1997).

14. Schulz, T. M., Whitehead, H., Gero, S. & Rendell, L. Overlapping and matching of codas in vocal interactions between sperm whales: insights into communication function. An. Behav. 76, 1977-1988 (2008). 

15. Rendell, L. & Whitehead, H. Vocal clans in sperm whales (*Physeter macrocephalus*). Proc. R. Soc. B 270, 225-231 (2003).

16. Watkins, W.A., Moore, K.E., Clark, C.W. & Dahlheim, M.E. in Animal Sonar (eds Nachtigall, P.E. & Moore, P.W.B.) 99-107 (Plenum, 1988). 

17. Gero, S. On the Dynamics of Social Relationships and Vocal Communication between Individuals and Social Units of Sperm Whales. Ph.D. Thesis (Dalhousie University, 2012).

18. Slater, P.J. The cultural transmission of bird song. Trends Ecol Evol. 1, 94–97 (1986).

19. Filatova, O. A., Burdin, A. M. & Hoyt, E. Is killer whale dialect evolution random? Behav. Proc. 99, 34–41 (2013).

20. Rendell, L. & Whitehead, H. Do sperm whales share coda vocalizations? Insights into coda usage from acoustic size measurement. An. Behav. 67, 865-874 (2004).

21. Schulz, T. M., Whitehead, H., Gero, S. & Rendell, L. Individual vocal production in a sperm whale (*Physeter macrocephalus*) social unit. Mar. Mam. Sci. 27, 149-166 (2010).

22. Fayet, A. L., Tobias, J. A., Hintzen, R. E., & Seddon, N. Immigration and dispersal are key determinants of cultural diversity in a songbird population. Behav. Ecol. 25, 744-753 (2014).

23. Antunes, R. N. C. Variation in Sperm Whale (*Physeter macrocephalus*) Coda Vocalizations and Social Structure in the North Atlantic Ocean. PhD thesis. (University of St. Andrews, 2009).




#### *Note on the package name*
**balabm** stands for:

**bal**: the word root for "whale" in the native languages of the collaborators of this project:

*  bal yena (Filipino)

*  bal lena (Spanish)

*  bal  ena (Romanian)

*  bal   na (Hungarian)

*  bal eia (Portuguese)

* whal e    (English)

**abm**: the acronym for Agent-Based Models








### **Installation** 
The package can be installed from the repository or binary package: 

#####**Repository**

1. Launch R. 
2. Install and/or load the package ‘*devtools*’: 
```
#!r
if(!require(devtools)){install.packages('devtools'); library(devtools)} 
```
3. Install and load the package ‘*balabm*’: 
```
#!r
install_bitbucket(“maucantor/balabm”); library(balabm)
```

#####**Binary package**
1. Go to the [source code](https://bitbucket.org/maucantor/balabm/src) 
2. Download the zipped file balabm_1.1.zip.
3. Launch R.
4. Install '*balabm*' from the local folder (Packages>Install package from local zip files>…). 
5. Load ‘*balabm*’: 
```
#!r
library(balabm)
```


### **Running the models** 
1. Browse your local machine for the folder where R was installed and open the folder */library/balabm/*. 
2. The folder */abm/* contains the scripts for each of the ABM (Check README.txt).
3. Copy the content of the folder */abm/* to your working directory.
4. Open the R scripts for the agent-based models in the Text Editor in R or RStudio, or in another editor such as Notepad.
5. Load the package
6. Run the models.


### **Citation** 
If you make use of these models, please cite the original manuscript:

```
#!r
Cantor M, Shoemaker LG, Cabral R, Flores CO, Varga M, Whitehead H. 2015. 
Multilevel animal societies can emerge from cultural transmission. Nature Communications. 6:8091 doi: 10.1038/ncomms9091
```


and its related R package:
```
#!r
citation("balabm")
```