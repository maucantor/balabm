# Social learning at the social population level with homophily effect and symbolic marking

# Summary -----------------------------------------------------------------
# This model follows the same structure of the social learning models (combination of abm_6 and abm_12). Now there is homophily effect and symbolic marking. The homophily effect make calves learn from individuals who has high similar coda repertoires. Symbolic marking is the display of a particular behavior variant used to define the social group identity. In this model, when calves are born they always receive a specific subset of coda types that is performed 100% of the time. Calves can learn from any individual in the population, but will also learn from the unit who have the highest similarity with their own unit. Use the learning process "sample" (highly recommended), with which calves can copy a given proportion of the adult's coda repertoire. Optionally, calves can also have individual learning. Agents are born, form social units each year, and die according to random processes.

# 1. Model initial setup --------------------------------------------------
library(balabm)

setup <- initial_setup(
# Running parameters
 runs = 1, export = TRUE, filename = "abm18",
# Population parameters
 times = 700, rem = 1, c = 0.05, intPop = 1000, nCodas = 62, nonzero = 0.5, preclan = FALSE, splitunit = TRUE, limit = "double",
# Learning parameters
 ilearn_rate = 0.02, learn_process = "sample", prop_unit = 0.1, prop_clan = 0, prop_pop = 0.9,
# Transmission parameters
 homophily = TRUE, intensity = NULL, anticonformism = FALSE, subset = 6, symbol_level = "unit",
# Modularity parameters
 similarity = "braycurtis", modularity = "walktrap", iter = 100, nullmodel = 5
)

source('do_initial_setup.R')

# 2. Model core setup -----------------------------------------------------
for (r in 1:runs) {
  source('do_model_setup.R')

# 3. Run one replicate ----------------------------------------------------
  for (t in 1:(times-1)) {
    songs_temp2 <- songs[,t]
    
    # birth rate (Whitehead 2002)
    birth <- (0.257-0.0038*age[,t])/2
    
    # Determine which agent will give birth 
    offspring <- rep(NA, (intPop*2))
    offspring <- ifelse( (age[,t] > 8 & age[,t] < 42 & yearsSinceBirth[,t] > 4), runif((intPop*2), min=0, max=1), 0)
    offspring2 <- rep(NA, (intPop*2))
    offspring2 <- ifelse((offspring > 0 & offspring < birth), 1, 0)
    
    # total number of births
    totalOffspring <- sum(offspring2[!is.na(offspring2)])
    years_temp <- ifelse(offspring2==1, 0, yearsSinceBirth[,t])
    
    # Assign coda repertoire to offspring
    offspringIndex <- which(offspring2==1)
    Youngsongs_temp <- rep(NA, nCodas*totalOffspring)
    indCoda <- rep(NA, nCodas)
    a <- 1
    
    # Newborns' learning
    for (i in offspringIndex) {
      
      # Determine what social unit the mother belongs to
      motherUnit <- unit[i,t]
      inUnit <- which(unit[,t]==motherUnit)
      unitSongs <- rep(0, nCodas)
      
      # Determine the average songs in the unit
      for (s in inUnit) {
        unitSongs <- unitSongs + songs[((s-1)*nCodas+1):((s-1)*nCodas+nCodas),t]
      }
      unitSongs <- unitSongs/length(inUnit)
      
      # Newborn calves' social learning process
      if (r==1){ 
        Youngsongs_temp[((a-1)*nCodas+1):((a-1)*nCodas+nCodas)] <- unitSongs
      } else {
        # Homophily effect: copying from the unit who has the highest coda similarity (first run is used to generate similarities)
        if(learn_process == "sample"){
          Youngsongs_temp[((a-1)*nCodas+1):((a-1)*nCodas+nCodas)] <- learn_by_sampling(prop_unit, prop_pop, prop_clan, homophily=TRUE, auxsongs = songs[,t], auxunit = unit[,t], auxclan = clan[,t])
        } 
        if(learn_process == "fraction"){
          unitSongs <- ifelse (unitSongs < 5, 0, unitSongs)
          popSongs <- songs[sample(which(!is.na(songs[,t])), nCodas, replace=F),t]
          calfUnit <- motherUnit
          maxsim <- which.max(songDistanceMatrix[calfUnit,])
          Youngsongs_temp[((a-1)*nCodas+1):((a-1)*nCodas+nCodas)] <- 0.1*unitCodas[,maxsim] + 0.9*popSongs
        }
      }
      
        # Symbolic Marking (defines symbols in the first run to be used in the subsequent ones)
        if(r==1){
          if(preclan==TRUE) stop(cat(paste("to run this model please set preclan=FALSE")))
          symbol_index <- symbolgenerator(subset, symbol_level)
        } else {
          symbolicmarking <- symbolic_marking(repertoire=Youngsongs_temp, symbol=symbol_index, symbol_level, preclan)
          Youngsongs_temp <- symbolicmarking$repertoire
        }  
      
        # Newborns' individual learning
        if ((as.integer(nCodas * ilearn_rate)) > 0){
          ilearn <- runif((as.integer(nCodas * ilearn_rate)), 1, 100)
          mutation <- as.integer(runif((as.integer(nCodas * ilearn_rate)), 1, nCodas))
          for (k in 1:length(mutation)){
            songs_temp2[(a-1)*nCodas + mutation[k]] <- ilearn[k]
          }
        }
      a <- a + 1
    }
    
    # 1-year old calves learning
    age1index <- which(age[,t]==1)
    for (o in age1index) {
      
      individualUnit <- unit[o,t]
      inUnit <- which(unit[,t]==individualUnit)
      unitSongs <- rep (0, nCodas)
      for (s in inUnit) {
        unitSongs <- unitSongs + songs[((s-1)*nCodas+1):((s-1)*nCodas+nCodas),t]
      }
      unitSongs <- unitSongs/length(inUnit)
      
      # 1-year old calves' social learning process
      if (r==1){ 
        songs_temp2[((o-1)*nCodas+1):((o-1)*nCodas+nCodas)] <- 0.5*unitSongs + 0.5*songs_temp2[((o-1)*nCodas+1):((o-1)*nCodas+nCodas)]
      } else {
        # Homophily effect
        if(learn_process == "sample"){
          songs_temp2[((o-1)*nCodas+1):((o-1)*nCodas+nCodas)] <- learn_by_sampling(prop_unit, prop_pop, prop_clan, homophily=TRUE, auxsongs = songs[,t], auxunit = unit[,t], auxclan = clan[,t])
        } 
        if(learn_process == "fraction"){
          unitSongs <- ifelse (unitSongs < 10, 0, unitSongs)
          popSongs <- songs[sample(which(!is.na(songs[,t])), nCodas, replace=F),t]
          calfUnit <- motherUnit
          maxsim <- which.max(songDistanceMatrix[calfUnit,])
          songs_temp2[((o-1)*nCodas+1):((o-1)*nCodas+nCodas)] <- 0.5*(0.1*unitCodas[,maxsim] + 0.9*popSongs) + 0.5*songs_temp2[((o-1)*nCodas+1):((o-1)*nCodas+nCodas)]      
        }
      }
      
        # 1-year old calves' individual learning
        if ((as.integer(nCodas * ilearn_rate)) > 0){
          ilearn <- runif((as.integer(nCodas * ilearn_rate)), 1, 100)
          mutation <- as.integer(runif((as.integer(nCodas * ilearn_rate)), 1, nCodas))
          for (k in 1:length(mutation)){
            songs_temp2[(o-1)*nCodas + mutation[k]] <- ilearn[k]
          }
        }
    }
    
    # 2-year old calves learning
    age2index <- which(age[,t]==2)
    for (o in age2index) {
      
      individualUnit <- unit[o,t]
      inUnit <- which(unit[,t]==individualUnit)
      unitSongs <- rep (0, nCodas)
      for (s in inUnit) {
        unitSongs <- unitSongs + songs[((s-1)*nCodas+1):((s-1)*nCodas+nCodas),t]
      }
      unitSongs <- unitSongs/length(inUnit)
      
      # 2-year old calves' social learning process
      if (r==1){
        songs_temp2[((o-1)*nCodas+1):((o-1)*nCodas+nCodas)] <- (2/3)*unitSongs + (1/3)*songs_temp2[((o-1)*nCodas+1):((o-1)*nCodas+nCodas)]
      } else {
        # Homophily effect
        if(learn_process == "sample"){
          songs_temp2[((o-1)*nCodas+1):((o-1)*nCodas+nCodas)] <- learn_by_sampling(prop_unit, prop_pop, prop_clan, homophily=TRUE, auxsongs = songs[,t], auxunit = unit[,t], auxclan = clan[,t])
        } 
        if(learn_process == "fraction"){
          unitSongs <- ifelse (unitSongs < 20, 0, unitSongs)
          popSongs <- songs[sample(which(!is.na(songs[,t])), nCodas, replace=F),t]
          calfUnit <- motherUnit
          maxsim <- which.max(songDistanceMatrix[calfUnit,])
          songs_temp2[((o-1)*nCodas+1):((o-1)*nCodas+nCodas)] <- (2/3)*(0.10*unitCodas[,maxsim] + 0.90*popSongs) + (1/3)*songs_temp2[((o-1)*nCodas+1):((o-1)*nCodas+nCodas)]
        }
      }
      
      # 2-year old calves' individual learning
      if ((as.integer(nCodas * ilearn_rate)) > 0){
        ilearn <- runif((as.integer(nCodas * ilearn_rate)), 1, 100)
        mutation <- as.integer(runif((as.integer(nCodas * ilearn_rate)), 1, nCodas))
        for (k in 1:length(mutation)){
          songs_temp2[(o-1)*nCodas + mutation[k]] <- ilearn[k]
        }
      }
    }
    
    # Assign offspring to the mother's unit
    offspringUnit <- unit[offspringIndex,t]
    
    # Determine which agents will randomly change social units
    change <- rep(NA, (intPop*2))
    change <- ifelse(age[,t] > 3, runif((intPop*2), min=0, max=1), 0)
    ageDep <- change[!is.na(change)] * ((1+age[!is.na(age[,t]),t])*0.1)
    change[1:length(ageDep)] <- ageDep
    change2 <- ifelse((change > 0 & change < c), 1, 0)
    unit_change <- ifelse(change2==1, round(runif((intPop*2), min=1, max=tvu)), unit[,t] )
    
    # Determine which agents die
    tempAge <- age[,t]
    alive <- length(tempAge[!is.na(tempAge)])
    death <- ((totalOffspring+alive)-intPop)/(totalOffspring+alive)
    #      #1.1 density-dependent, age-independent
    #        whalesDie <- ifelse(age[,t] >=0, runif(length(age[,t]), min=0, max=1), NA)
    #        whalesDie2 <- ifelse(whalesDie < death, 1, 0)
    #      #1.2. density-dependent, age-dependent: older agents have higher mortality probability 
    #        whalesDie <- ifelse(age[,t] >=0, runif(length(age[,t]), min=0, max=1), NA)
    #        ageDepend <- whalesDie[!is.na(whalesDie)] / ((1+age[!is.na(age[,t]),t])*0.1)
    #        whalesDie[1:length(ageDepend)] <- ageDepend
    #        whalesDie2 <- ifelse(whalesDie < death, 1, 0)
    #      #1.3. density-dependent, age-dependent: younger agents have higher mortality rate  
    #        whalesDie <- ifelse(age[,t] >=0, runif(length(age[,t]), min=0, max=1), NA)
    #        ageDepend <- whalesDie[!is.na(whalesDie)] * ((1+age[!is.na(age[,t]),t])*0.1)
    #        whalesDie[1:length(ageDepend)] <- ageDepend
    #        whalesDie2 <- ifelse(whalesDie < death, 1, 0)
            #1.4. density-dependent, empirical age-dependence mortality rate 
            howManyDie <- round(death*alive)
            #calf=0.093; adults+juveniles=0.055 (IWC 1982)
            probDeath <- ifelse(age[,t]<=2, 0.093, 0.055); probDeath <- probDeath[!is.na(probDeath)] 
            #calf=0.1679; adults+juveniles=0.0505 (Whitehead & Gero 2015)
            #probDeath <- ifelse(age[,t]<=2, 0.1679, 0.0505); probDeath <- probDeath[!is.na(probDeath)] 
            indexDie <- c(1:alive)                    
            whoDie <- sample(indexDie, size=howManyDie, replace=FALSE, prob=probDeath) 
            whalesDie2 <- rep(0, alive)
            whalesDie2[whoDie]=1
                
    # Update all info for agents that lived
    age_temp <- unit_temp <- yearsSinceBirth_temp <-rep (NA, (intPop*2))
    age_temp <- ifelse(whalesDie2==0, age[,t] + 1, NA )
    unit_temp <- ifelse(whalesDie2==0, unit_change, NA )
    yearsSinceBirth_temp <- ifelse(whalesDie2==0, years_temp + 1, NA )
    songs_temp <- rep (NA, 300*nCodas)
    whalesDie2_songs <- rep(whalesDie2, each=nCodas)
    songs_temp <- ifelse(whalesDie2_songs==0, songs_temp2, NA) 
    
    # Add in agents born in year t
    age_total <- songs_total <- unit_total <- yearsSinceBirth_total <- NULL
    age_total <- c(age_temp, rep(0,totalOffspring))
    age_total <- age_total[!is.na(age_total)]
    unit_total <- c(unit_temp, offspringUnit)
    unit_total <- unit_total[!is.na(unit_total)]
    yearsSinceBirth_total <- c(yearsSinceBirth_temp, rep(0,totalOffspring))
    yearsSinceBirth_total <- yearsSinceBirth_total[!is.na(yearsSinceBirth_total)]
    songs_total <- c(songs_temp, Youngsongs_temp)
    songs_total <- songs_total[!is.na(songs_total)]
    
    # Overal update
    age[1:length(age_total),t+1] <- age_total
    yearsSinceBirth[1:length(yearsSinceBirth_total), t+1] <- yearsSinceBirth_total
    songs[1:length(songs_total), t+1] <- songs_total
    if(splitunit==FALSE){
      unit[1:length(unit_total),t+1] <- unit_total
    } else {
      unit[1:length(unit_total),t+1] <- split_units(unit_total, limit)
      tvu <- max(unit_total)
    }  
    
  }
  
# 4. Simulated data analyses ----------------------------------------------
  source('do_modularity_updates.R')
  source('do_modularity_analyses.R')
  source('do_similarities.R')
}                                           

# 5. Final processing, outputs and plottings ------------------------------
source('do_final_processing.R')
alarm()
final <- print_output(codasim, codasimclan, simoutput, modoutput, export)
final
source('do_plotting.R')