Package: balabm
Type: Package
Title: Agent-based models for sperm whale societies
Version: 1.0
Date: 2013-10-28
Author: Mauricio Cantor
Maintainer: Mauricio Cantor <mauricio.cantor@dal.ca>


This folder contains the scripts for all agent-based models. Scripts are quite repetitive but were written to be self explanatory.

To run the models, copy all files in {.../inst/abm/} to your working directory. Make sure you have installed the package {balabm}

File names follow the format: Model name_learning process_learning bias_social level, where
* Model name: "abm" 1 to 20
* Learning process: "indlearn" individual learning; "genetic" vertical genetic transmission; "soclearn" social learning
* Learning bias: "hom" homophily; "conf" conformism; "symb" symbolic marking
* Social level: "pop" population; "unit" social unit; "clan" predefined geographical vocal clan


abm01_indlearn
abm02_genetic
abm03_soclearn_pop
abm04_soclearn_unit
abm05_soclearn_clan
abm06_soclearn_hom_pop
abm07_soclearn_hom_unit
abm08_soclearn_hom_clan
abm09_soclearn_conf_pop
abm10_soclearn_conf_unit
abm11_soclearn_conf_clan
abm12_soclearn_symb_pop
abm13_soclearn_symb_unit
abm14_soclearn_symb_clan
abm15_soclearn_homconf_pop
abm16_soclearn_homconf_unit
abm17_soclearn_homconf_clan
abm18_soclearn_homsymb_pop
abm19_soclearn_homsymb_unit
abm20_soclearn_homsymb_clan