READ ME

This folder contains two files with the empirical data used in the following article:

Multilevel animal societies can emerge from cultural transmission

Mauricio Cantor 1*, Lauren G. Shoemaker 2, Reniel B. Cabral 3, 4, Cesar O. Flores 5, Melinda Varga 6, Hal Whitehead 1

1 Department of Biology, Dalhousie University, 1355 Oxford Street, Halifax, NS B3H 4J1, Canada.
2 Department of Ecology and Evolutionary Biology, University of Colorado at Boulder, UCB 334, Ramaley Hall, Boulder, CO 80309, U.S.A.
3 National Institute of Physics, University of the Philippines Diliman, Quezon City 1101, Philippines.
4 Bren School of Environmental Science and Management and Marine Science Institute, University of California Santa Barbara, CA 93106, U.S.A.
5 School of Physics, Georgia Institute of Technology, 837 State Street, Atlanta, Georgia 30332-430, U.S.A.
6 Department of Physics, University of Notre Dame, 225 Nieuwland Science Hall, Notre Dame, IN 46556, U.S.A.
* Corresponding author: Email: m.cantor@ymail.com, Telephone: +1 (902) 494-3723

The files are organized in edge lists, i.e. non-zeroed elements of the adjacency matrices of the networks plotted
in the Figure 1 of this article. A edge list is a three-column matrix in which the two first are the nodes of the 
network, linked by the weighted edges given in the third column. In the file "edgelist_inds_hwi.txt" the nodes 
are photoidentified individual whales and the edge weights are given by the half-weight association index 
(estimated of the proportion of times individuals were seen together). In the "edgelist_units_codasim.txt" file, 
the nodes are social units of whales and the edge weights are the multivariate similarity of their coda repertoires. 
These data were originally used in the following articles and must be referenced whenever these data is used:

Association data (edgelist_inds_hwi.txt):
Christal, J., Whitehead, H. & Lettevall, E. Sperm whale social units: variation and change. Can. J. Zool. 76, 1431-1440 (1998).
Acoustic data (edgelist_units_codasim.txt):
Rendell, L. & Whitehead, H. Vocal clans in sperm whales (Physeter macrocephalus). Proc. R. Soc. B 270, 225-231 (2003).